package controleur;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.mygdx.game.MyGdxGame;

import modele.Banana;
import modele.Barrel;
import modele.DonkeyKong;
import modele.Mario;
import vue.LooseScreen;
import vue.TextureFactory;

public class HandleDonleyKong {
	
	private ArrayList<Banana> bananas = new ArrayList<Banana>();
	private float lastTimeBanana = 0;
	private ArrayList<Barrel> barrels= new ArrayList<Barrel>();
	private ArrayList<Barrel> barrelSides= new ArrayList<Barrel>();
	private float lastTimeBarrel = 0;
	private float lastTimeBarrelSide = 0;
	private float cooldownBarrelSide;
	private float lastTimeAnimation =0;
	private float cpt =0;
	private double rand;
	Random r = new Random();
	private MyGdxGame game;
	private boolean marioExplosion =false;
	private int cptExplosion =0;
	private float lastTimeExplosion =0;
	
	private DonkeyKong donkeyKong;
	private Mario mario;
	
	public HandleDonleyKong(DonkeyKong donkeyKong, Mario mario,MyGdxGame game) {
		this.donkeyKong = donkeyKong;
		this.mario = mario;
		this.game = game;
	}
	
	public ArrayList<Banana> getBananas(){
		return bananas;
	}
	
	public ArrayList<Barrel> getBarrels(){
		return barrels;
	}
	
	public ArrayList<Barrel> getBarrelsSide(){
		return barrelSides;
	}
	
	public void moveDonkeyKong() {
		cpt+=Gdx.graphics.getDeltaTime();
		Random random = new Random();
		if(cpt>=donkeyKong.getDeltaT()) {
			cpt=0;
			rand = (Math.random() * 1);
		}
		if(rand<=0.5) {
			if(donkeyKong.getX() >= 800) {
				rand = 0.7;
				donkeyKong.setX(800);
			}
			else {
				donkeyKong.moveRight();
			}
		}
		else {
			if(donkeyKong.getX() <= 100) {				
				donkeyKong.setX(100);
				rand = 0.3;
			}
			else {
				donkeyKong.moveLeft();
			}
		}	
	}
	
	public void throwBanana( ) {
		lastTimeBanana +=Gdx.graphics.getDeltaTime();
		if(lastTimeBanana >=1.5) {
			lastTimeBanana =0;
			Banana banana1 = new Banana(2,donkeyKong.getX()+donkeyKong.getWidth()/2,donkeyKong.getY());
			bananas.add(banana1);
		}
		Iterator<Banana> it = bananas.iterator();
	    while(it.hasNext()) {
	      Banana banana = it.next();
	      banana.moveDown();
		    if(banana.getY()+30 < 0  ) {
		    	it.remove();
		    	
		    }
		    if(banana.getRectangle().overlaps(mario.getRectangle())){
		    	it.remove();
		    	mario.looseHealth(1);
		    	if(mario.getHealth()==0) {
		    		mario.resetHealth();
		    		mario.setX(400);
		    		mario.setY(49);
		    		
		    	}
		    }
	    }
	}

	
	public void throwBarrelVertical() {
		
		lastTimeBarrel +=Gdx.graphics.getDeltaTime();
		if(mario.getX() >= donkeyKong.getX() && mario.getX()+mario.getWidth() <= donkeyKong.getX()+donkeyKong.getWidth() && lastTimeBarrel>donkeyKong.getCooldownBarrel()) {
			lastTimeBarrel =0;
			Random r = new Random();
			int speed = (int) (donkeyKong.getMinSpeedBarrel() + r.nextInt((int)donkeyKong.getMaxSpeedBarrel() - (int)donkeyKong.getMinSpeedBarrel()));
			Barrel barrel= new Barrel(1,speed,donkeyKong.getX()+donkeyKong.getWidth()/2,donkeyKong.getY());
			barrels.add(barrel);
		}
		Iterator<Barrel> it = barrels.iterator();
	    while(it.hasNext()) {
	      Barrel barrel1 = it.next();
	      barrel1.moveDown();
	      if(barrel1.getRectangle().overlaps(mario.getRectangle())) {
				it.remove();
				mario.looseHealth(1);
				marioExplosion = true;
				if(mario.isDead()) {
					game.setScreen(new LooseScreen(game));
				}
			}
		    if(barrel1.getY()+30 < 0  ) {
		    	it.remove();
		    	
		    }
	
	    }
	}
	
	public void throwBarrelRandom() {
		
		if(cooldownBarrelSide == 0) {
			cooldownBarrelSide = (r.nextInt(donkeyKong.getCooldownBarrel()));			
		}
		lastTimeBarrelSide +=Gdx.graphics.getDeltaTime();
		if(lastTimeBarrelSide > cooldownBarrelSide) {			
			int speed = (int) (donkeyKong.getMinSpeedBarrel() + r.nextInt((int)donkeyKong.getMaxSpeedBarrel() - (int)donkeyKong.getMinSpeedBarrel()));
			int angle = (r.nextInt(180-0));
			
			barrelSides.add(new Barrel(2, speed, donkeyKong.getX()+donkeyKong.getWidth()/2, donkeyKong.getY(),Math.toRadians(angle)));
			cooldownBarrelSide =0;
			lastTimeBarrelSide=0;
		}
		Iterator<Barrel> it = barrelSides.iterator();
		while(it.hasNext()) {
			Barrel barrel1 = it.next();
			barrel1.moveWithAngle();
			if(barrel1.getRectangle().overlaps(mario.getRectangle())) {
				it.remove();
				mario.looseHealth(1);
				marioExplosion = true;
				if(mario.isDead()) {
					game.setScreen(new LooseScreen(game));
				}
			}
			if(barrel1.getY()+30 < 0 || barrel1.getX()<0 || barrel1.getX() > Gdx.graphics.getWidth()  ) {
				it.remove();
				
			}
			
		}
    }
	
	public void marioExplode() {
		if(cptExplosion ==4) {
			cptExplosion=0;
			marioExplosion = false;
		}
		String nom = "tile00"+cptExplosion+".png";
		game.getSpriteBatch().draw(TextureFactory.getInstance().getTextureExplosion(nom), mario.getX()-50, mario.getY()-50,mario.getWidth()*3,mario.getHeight()*3);
		lastTimeExplosion +=Gdx.graphics.getDeltaTime();
		if(lastTimeExplosion > 0.2) {
			lastTimeExplosion =0;
//			nom = "tile00"+cptExplosion+".png";
			System.out.println(nom);
			cptExplosion+=1;
		}
	}
	
	public boolean marioIsExploding() {
		return marioExplosion;
	}
	

	public void animation() {
		lastTimeAnimation +=Gdx.graphics.getDeltaTime();
		if(lastTimeAnimation > donkeyKong.getAnimationSpeed()) {
			donkeyKong.increaseCpt();
			lastTimeAnimation=0;
		}
		
	}
}
