package controleur;



import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import javax.naming.NoPermissionException;
import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MoveAction;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.mygdx.game.MyGdxGame;

import modele.Banana;
import modele.Barrel;
import modele.DonkeyKong;
import modele.GameElement;
import modele.Hammer;
import modele.Mario;
import modele.Platform;
import modele.SingleLadder;
import modele.Chest;
import modele.World;
import vue.LooseScreen;
import vue.SuccessScreen;
import vue.TextureFactory;

public class WorldRenderer{
	private World world;
	private MyGdxGame game;
	private Mario mario;
	private DonkeyKong donkeyKong;
	private ArrayList<Platform> platforms = new ArrayList<Platform>();
	private ArrayList<SingleLadder> singleLadders =  new ArrayList<SingleLadder>();
	private ArrayList<Chest> chests = new ArrayList<Chest>();
	private ArrayList<Hammer> hammers = new ArrayList<Hammer>(); 
	HandleDonleyKong handleDonleyKong;
	private float lastTimeAnimation =0;
	private BitmapFont potentiel = new BitmapFont();
	private int score=0;
	private Sound sond = Gdx.audio.newSound(Gdx.files.internal("0477.mp3"));
	private int cptExplosion =0;
	private float lastTimeExplosion =0;
	private boolean donkeyKongExplosion =false;
	private double angleShooting =0;
	private boolean marioShooting = false;
	private ShapeRenderer shape = new ShapeRenderer();

	
	
	
	public WorldRenderer(MyGdxGame game) {
		this.game = game;
		world = new World();
		for(GameElement gameElement : world.getGameElements()) {
			if(gameElement instanceof Mario)
				mario = (Mario)gameElement;
			else if (gameElement instanceof Platform)
				platforms.add((Platform)gameElement);
			else if (gameElement instanceof SingleLadder) 
				singleLadders.add((SingleLadder)gameElement);
			else if(gameElement instanceof DonkeyKong)
				donkeyKong=(DonkeyKong)gameElement;
		}
		handleDonleyKong = new HandleDonleyKong(donkeyKong,mario,game);
		placeChest(mario.getNbCaisses());
		
	}
	
	public void placeChest(int nb) {
		Random rand = new Random();
		int n = platforms.size()-1;
		for(int i =0; i < nb; i++) {
			 int nombreAleatoire = rand.nextInt(n + 1) ;
			 Platform platform = platforms.get(nombreAleatoire);
			 chests.add(new Chest(0, platform.getX(), platform.getY()+40));
		}
	}
	
	public void render(SpriteBatch batch) {
		this.handleInput();
		handleDonleyKong.moveDonkeyKong();
		handleDonleyKong.throwBarrelVertical();
		handleDonleyKong.throwBarrelRandom();
		handleDonleyKong.animation();
		this.marioAnimation();
//		System.out.println(Gdx.graphics.getFramesPerSecond());
		batch.begin();
		potentiel.draw (batch, "Potentiel : "+mario.getPotentiel(), Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()-10);
		mario.update();
		for(GameElement  gElement :  world.getGameElements() ) {
			drawTexture(gElement, batch);
		}
		for(Hammer hammer: hammers) {
			drawTexture(hammer, batch);
		}
		for (Barrel barrel : handleDonleyKong.getBarrels()) {
			drawTexture(barrel, batch);
		}
		for (Barrel barrel : handleDonleyKong.getBarrelsSide()) {
			drawTexture(barrel, batch);
		}
		for(Chest chest : chests) {
			drawTexture(chest, batch);
		}
		if(handleDonleyKong.marioIsExploding()) {
			handleDonleyKong.marioExplode();
		}
		if(donkeyKongExplosion) {
			donkeyKongExplode();
		}
		drawHealth(batch);
		
		

		
		batch.end();
		shape.begin(ShapeType.Line);
		shape.setColor(1, 0, 0, 1); 
		double a = Math.toRadians(angleShooting);
		shape.line(mario.getX(),mario.getY(),(float)(mario.getX()+mario.getHeight()*Math.cos(a)*2),(float)(mario.getY()+mario.getWidth()*Math.sin(a)*2));
		shape.end();
		
	}
	
	public void donkeyKongExplode() {
		if(cptExplosion ==4) {
			cptExplosion=0;
			donkeyKongExplosion = false;
		}
		String nom = "tile00"+cptExplosion+".png";
		game.getSpriteBatch().draw(TextureFactory.getInstance().getTextureExplosion(nom), donkeyKong.getX()-75, donkeyKong.getY()-75,donkeyKong.getWidth()*2,donkeyKong.getHeight()*2);
		lastTimeExplosion +=Gdx.graphics.getDeltaTime();
		if(lastTimeExplosion > 0.2) {
			lastTimeExplosion =0;
			nom = "tile00"+cptExplosion+".png";
			System.out.println(nom);
			cptExplosion+=1;
		}
	}
	
	public void drawHealth(SpriteBatch batch) {
		int cpt = 0;
		for(int i=0; i<mario.getHealth(); i++) {
			batch.draw(TextureFactory.getInstance().getHeart(), 10+cpt, Gdx.graphics.getHeight() - 30,30,30);
			cpt+=35;
		}
		cpt=0;
		for(int i=0; i<donkeyKong.getHealth(); i++) {
			batch.draw(TextureFactory.getInstance().getTinyMonkey(), Gdx.graphics.getWidth()-45-cpt, Gdx.graphics.getHeight() - 45,60,60);
			cpt+=35;
		}
	}
	

	public void drawTexture(GameElement gElement,SpriteBatch batch) {
		batch.draw(TextureFactory.getInstance().getTexture(gElement.getName()),gElement.getX(), gElement.getY(),gElement.getWidth(),gElement.getHeight());
	}


	private void handleInput() {
		moveMario();
		if(Gdx.input.isKeyJustPressed(Keys.SPACE)) {
			marioShooting = true;
			
	    }
		if(Gdx.input.isKeyPressed(Keys.SPACE)) {
			angleShooting+=Gdx.graphics.getDeltaTime()*120;
			angleShooting = angleShooting %180;
		}
		if(!Gdx.input.isKeyPressed(Keys.SPACE) && marioShooting) {
			System.out.println(angleShooting);
			Hammer hammer= new Hammer(mario.getHammerspeed(),mario.getX()+mario.getWidth()/2,mario.getY()+mario.getHeight()/2,Math.toRadians(angleShooting));
			hammers.add(hammer);
			mario.loosePotentiel(1);
			marioShooting = false;
			angleShooting =0;
		}
		
		handleShoot();
		
	}
	
	private void handleShoot() {
		Iterator<Hammer> it = hammers.iterator();
		while(it.hasNext()) {
			Hammer hammer1 = it.next();
			hammer1.moveWithAngle();

			{				
			if(hammer1.getRectangle().overlaps(donkeyKong.getRectangle())) {
				it.remove();
				if(mario.getPotentiel()>=mario.getSeuilPotentiel()) {
					donkeyKong.looseHealth(1);
					donkeyKongExplosion = true;
					if(donkeyKong.isDead()) {
						game.setScreen(new SuccessScreen(game));
					}
				}
			}
			else if(hammer1.getY()+30 > Gdx.graphics.getHeight()  ) {
				it.remove();	    	
			}  
			}
		}
	}
	
	private void marioAnimation() {
		lastTimeAnimation +=Gdx.graphics.getDeltaTime();
		if(lastTimeAnimation > mario.getAnimationSpeed()) {
			mario.increaseCpt();
			lastTimeAnimation=0;
		}
	}
	
	private void moveMario() {
		collideWithChest();
		if(mario.getX() < 0) mario.setX(0);
	    if(mario.getX() > Gdx.graphics.getWidth() - 50) mario.setX(Gdx.graphics.getWidth() - mario.getWidth());
	
	    if(Gdx.input.isKeyJustPressed(Keys.LEFT)) {
	    	mario.setOrientation(false);
	    }
	    if(Gdx.input.isKeyJustPressed(Keys.RIGHT)) {	
			mario.setOrientation(true);			
		}
	    if(Gdx.input.isKeyPressed(Keys.LEFT)) {
	    	if(marioOnPlatform()) {
	    		mario.setOnLadder(false);
	    		mario.moveLeft();		    	
		    }
	    	else {
	    		sond.play();
	    		mario.setFalling(true);
	    	}
    	}
	    if(Gdx.input.isKeyPressed(Keys.RIGHT)) {
	    	if(marioOnPlatform()) {
	    		mario.setOnLadder(false);
	    		mario.moveRight();		    	
		    }
	    	else {
	    		sond.play();
	    		mario.setFalling(true);
	    	}

    	}
	    if(mario.isFalling()) {
	    	this.fall();
	    }
	    
	    int indexSingleLadder = marioOnSingleLadder();
	    if(indexSingleLadder != -1) {
	    	if(Gdx.input.isKeyPressed(Keys.UP)) {
	    		canMoveSingleLadder(indexSingleLadder,"up");
	    		mario.setOnLadder(true);
	    	}
	    	if(Gdx.input.isKeyPressed(Keys.DOWN)) {
	    		canMoveSingleLadder(indexSingleLadder,"down");
	    		mario.setOnLadder(true);
	    	}	    	
	    }
	    if(Gdx.input.isKeyJustPressed(Keys.A)) {
	    	System.out.println(mario.getRectangle());
	    }
	}
	
	private void collideWithChest() {
		Iterator<Chest> it = chests.iterator();
		while(it.hasNext()) {
			Chest chest = it.next();
			if(mario.getRectangle().overlaps(chest.getRectangle())) {
				it.remove();
				mario.increasePotentiel(10);
				placeChest(1);
				break;
			}
		}
	}
	
	private void fall() {
		boolean onPlat = false;
		while(!onPlat) {
			for (Platform platform : platforms) {
				if(mario.getRectangle().overlaps(platform.getRectangle())) {
					mario.setY(platform.getY()+platform.getHeight()-1);
					onPlat = true;
					mario.setFalling(false);
					break;
				}
			}
			onPlat=true;
		}
	}

	
	private void canMoveSingleLadder(int i,String direction) {
		if(direction.equals("up")) {
			canMoveUp(i);
		}
		else {
			canMoveDown(i);
		}
	}
	
	private void canMoveUp(int i) {
		SingleLadder singleLadder = singleLadders.get(i);
		if( !((mario.getX()-10 >=  singleLadder.getX() || mario.getX() +10 <= singleLadder.getX()))) {
			if(overlapsTwoLadder(i,"up")) {
				mario.moveUp();
				return;
			}
			float d = mario.getY()+100*Gdx.graphics.getDeltaTime()*mario.getSpeed();
			if(mario.getY()+5 >= singleLadder.getY()+singleLadder.getHeight()) {
				mario.setY(singleLadder.getY()+singleLadder.getHeight()-1);
			}
			else {
				mario.moveUp();
			}
		}	
	}
	
	private void canMoveDown(int i) {
		SingleLadder singleLadder = singleLadders.get(i);
		if(!((mario.getX()-10 >=  singleLadder.getX() || mario.getX() +10 <= singleLadder.getX()))) {
			if(overlapsTwoLadder(i,"down")) {
				mario.moveDown();
				return;
			}
			float d = mario.getY()-100*Gdx.graphics.getDeltaTime()*mario.getSpeed();
			if(mario.getY() <= singleLadder.getY()) {
				mario.setY(singleLadder.getY()-1);
			}
			else {
				mario.moveDown();
			}
		}
	}
	
	private int marioOnSingleLadder() {
		for(int i=0; i<singleLadders.size(); i++) {
			if(mario.getRectangle().overlaps(singleLadders.get(i).getRectangle())) 
				return i;
		}
		return -1;
	}

	
	private boolean overlapsTwoLadder(int i,String direction) {
		if(direction.equals("up")) {
			for (int j = i + 1; j < singleLadders.size(); j++) {
				SingleLadder singleLadder2 = singleLadders.get(j);
				if (mario.getRectangle().overlaps(singleLadder2.getRectangle()))  {
					return singleLadders.get(i).getX() == singleLadder2.getX();
				}
			} 
			return false;			
		}
		else {
			for(int j = i-1; j>=0; j--){
				SingleLadder singleLadder2 = singleLadders.get(j);
				if (mario.getRectangle().overlaps(singleLadder2.getRectangle()))  {
					return singleLadders.get(i).getX() == singleLadder2.getX();
				}
			} 
			return false;	
		}
	}


	private boolean marioOnPlatform() {
		for (Platform platform : platforms) {
			if(mario.getRectangle().overlaps(platform.getRectangle())){
				if( !(mario.getY() > platform.getY() + platform.getHeight() - 2))
					return false;
//				if(mario.getOrientation()) {
//					if(mario.getX()+mario.getWidth() <= platform.getX()+platform.getWidth())
//						return true;					
//				}
//				else {
//					if(mario.getX()-mario.getWidth() >= platform.getX()-platform.getWidth())
//						return true;
//				}
				return true;
			}
		}
		return false;
	}

	
	public void update(float delta) throws InterruptedException{	
		
	}
	
	
}
