package modele;

import com.badlogic.gdx.Gdx;

public class Hammer extends MovingObject{
	private double angle;
	

	public Hammer(float speed,float x,float y,double angle) {
		super(speed);
		rectangle.x = x;
		rectangle.y = y;
		rectangle.height = 30;
		rectangle.width = 30;
		this.angle = angle;
	}

	public void moveWithAngle() {
		setY(getY()+100*Gdx.graphics.getDeltaTime()*getSpeed()*(float)Math.sin(angle));
		setX(getX()+100*Gdx.graphics.getDeltaTime()*getSpeed()*(float)Math.cos(angle));
	}
	
	@Override
	public String getName() {
		return "hammer.png";
	}
}
