package modele;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DonkeyKong extends MovingObject{
	private int health;
	private float deltaT;
	private float minSpeedBarrel;
	private float maxSpeedBarrel;
	private int cooldownBarrel;
	private float animationSpeed;
	private int cpt =1;
	
	public DonkeyKong (
			@JsonProperty("x") float x, 
		      @JsonProperty("y") float y, 
		      @JsonProperty("width") float width, 
		      @JsonProperty("height") float height,
		      @JsonProperty("speed") float speed,
		      @JsonProperty("deltaT") float deltaT,
		      @JsonProperty("health") int health,
		      @JsonProperty("animationSpeed") float animationSpeed,
		      @JsonProperty("cooldownBarrel") int cooldownBarrel,
		      @JsonProperty("minSpeedBarrel") float minSpeedBarrel,
		      @JsonProperty("maxSpeedBarrel") float maxSpeedBarrel){
		super(speed);
		rectangle.x=x;
		rectangle.y=y;
		rectangle.width = width;
		rectangle.height =height;
		this.deltaT = deltaT;
		this.minSpeedBarrel = minSpeedBarrel;
		this.maxSpeedBarrel = maxSpeedBarrel;
		this.cooldownBarrel = cooldownBarrel;
		this.animationSpeed = animationSpeed;
		this.health = health;
	}
	
	
	public float getAnimationSpeed() {
		return animationSpeed;
	}


	public void setAnimationSpeed(float animationSpeed) {
		this.animationSpeed = animationSpeed;
	}


	public void increaseCpt() {
		this.cpt+=1;
		if(this.cpt == 7) {
			this.cpt=1;
		}
	}
	
	public float getMinSpeedBarrel() {
		return minSpeedBarrel;
	}




	public void setMinSpeedBarrel(float minSpeedBarrel) {
		this.minSpeedBarrel = minSpeedBarrel;
	}




	public float getMaxSpeedBarrel() {
		return maxSpeedBarrel;
	}




	public void setMaxSpeedBarrel(float maxSpeedBarrel) {
		this.maxSpeedBarrel = maxSpeedBarrel;
	}




	public int getCooldownBarrel() {
		return cooldownBarrel;
	}




	public void setCooldownBarrel(int cooldownBarrel) {
		this.cooldownBarrel = cooldownBarrel;
	}

	
	public int getHealth() {
		return health;
	}
	
	public boolean isDead() {
		return health==0;
	}
	
	public void looseHealth(int n) {
		health-=n;
	}


	public float getDeltaT() {
		return deltaT;
	}
	
	@Override
	public String getName() {
		return "dktitle0"+cpt+".png";
		
	}

}
