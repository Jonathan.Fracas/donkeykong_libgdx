package modele;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Orientation;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.fasterxml.jackson.annotation.JsonProperty;

import vue.TextureFactory;

public class Mario extends Player{
	private int health;
	private float hammerspeed;
	private float fallspeed;
	private float animationSpeed;
	private boolean isFalling =false;
	private boolean onLadder = false;
	private int cpt = 4;
	private float xStart;
	private float yStart;
	private int potentiel =0;
	private int seuilPotentiel;
	private int nbCaisses;
	
	public Mario (
			@JsonProperty("x") float x, 
		      @JsonProperty("y") float y, 
		      @JsonProperty("width") float width, 
		      @JsonProperty("height") float height,
		      @JsonProperty("speed") float speed,
		      @JsonProperty("fallspeed") float fallspeed,
		      @JsonProperty("health") int health,
		      @JsonProperty("nbCaisses") int nbCaisses,
		      @JsonProperty("seuilPotentiel") int seuilPotentiel,
		      @JsonProperty("animationSpeed") float animationSpeed,
		      @JsonProperty("hammerspeed") float hammerspeed,
		      @JsonProperty("orientation") int orientation){
		super(speed, orientation);
		rectangle.x=x;
		rectangle.y=y;
		rectangle.width = width;
		rectangle.height =height;
		this.hammerspeed = hammerspeed;
		this.fallspeed = fallspeed;
		this.animationSpeed = animationSpeed;
		this.xStart = x;
		this.yStart = y;
		this.health = health;
		this.seuilPotentiel =seuilPotentiel;
		this.nbCaisses = nbCaisses;
	}

	public String getName() {
		if(!isOnLadder()) {			
			if(getOrientation()) {
				return "MarioIdleRight.png";
			}
			else {
				return "MarioIdleLeft.png";
			}
		}
		else {
			return "mario_climbing"+cpt+".png";
		}
		
	}
	
	public void increaseCpt() {
		this.cpt+=1;
		if(this.cpt == 6) {
			this.cpt=4;
		}
	}
	
	public void update() {
		if(isFalling) {
			setY(getY()-100*Gdx.graphics.getDeltaTime()*getFallSpeed());
		}
	}
	
	public boolean isFalling() {
		return isFalling;
	}

	public void setFalling(boolean isFalling) {
		this.isFalling = isFalling;
	}

	public float getFallSpeed() {
		return fallspeed;
	}
	
	
	public float getHammerspeed() {
		return hammerspeed;
	}

	public void setHammerspeed(float hammerspeed) {
		this.hammerspeed = hammerspeed;
	}

	
	
	public boolean isOnLadder() {
		return onLadder;
	}

	public void setOnLadder(boolean onLadder) {
		this.onLadder = onLadder;
	}

	public int getHealth() {
		return health;
	}
	
	public boolean isDead() {
		return health==0;
	}
	
	public void looseHealth(int n) {
		health-=n;
	}
	
	public void gainHealth(int n) {
		health+=n;
	}
	
	public int getPotentiel() {
		return potentiel;
	}
	
	public int getSeuilPotentiel() {
		return seuilPotentiel;
	}
	
	public void loosePotentiel(int n) {
		potentiel-=n;
	}
	
	public void increasePotentiel(int n) {
		potentiel+=n;
	}

	public void resetHealth() {
		health = 4;
		setX(xStart);
		setY(yStart);
	}

	public float getAnimationSpeed() {
		return this.animationSpeed;
	}
	
	public int getNbCaisses() {
		return nbCaisses;
	}
}
