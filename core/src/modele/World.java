package modele;

import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import com.badlogic.gdx.assets.AssetLoaderParameters.LoadedCallback;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.annotation.JsonSubTypes;



public class World {
	private ArrayList<GameElement> gameElements = new ArrayList<GameElement>();
	
	public World(){
		initialize();
		for (GameElement gameElement : gameElements) {
			System.out.println(gameElement.rectangle);
		}
	}
	
	public void addGameElement(GameElement gameElement) {
		gameElements.add(gameElement);
	}

		
	public void initialize() {

		
		
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		try {
		    gameElements = mapper.readValue(new File("configuration.json"), mapper.getTypeFactory().constructCollectionType(
		            List.class, GameElement.class));
		} catch (IOException e) {
		    e.printStackTrace();
		}
	}
	
	
	public ArrayList<GameElement> getGameElements(){
		return gameElements;
	}
	
	public Mario getMario() {
		for (GameElement gameElement : gameElements) {
			if(gameElement instanceof Mario)
				return (Mario) gameElement;
		}
		return null;
	}

}
