package modele;

public class Chest extends UnmovingObject {

	public Chest(int number,float x, float y) {
		super(number);
		rectangle.x = x;
		rectangle.y =y;
		rectangle.width=50;
		rectangle.height =50;
	}

	@Override
	public String getName() {
		return "Chest00"+number+".png";
	}

}
