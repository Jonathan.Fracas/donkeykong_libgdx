package modele;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;

public abstract class Player extends MovingObject{
	
	
	boolean orientation;
	
	
	
	public Player(float speed, int orientation) {
		super(speed);
		if(orientation==1)
			this.orientation =true;
		else 
			this.orientation = false;
	}

	public boolean getOrientation() {
		return orientation;
	}

	public void setOrientation(boolean orientation) {
		this.orientation = orientation;
	}
	
}
