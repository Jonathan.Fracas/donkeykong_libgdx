package modele;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;

public abstract class MovingObject extends GameElement{
	float speed;

	public MovingObject(float speed) {
		super();
		this.speed = speed;
	}

	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}
	
	public void moveRight() {
		setX(getX()+100*Gdx.graphics.getDeltaTime()*getSpeed());
	}
	
	public void moveLeft() {
		setX(getX()-100*Gdx.graphics.getDeltaTime()*getSpeed());
	}
	
	public void moveUp() {
		setY(getY()+100*Gdx.graphics.getDeltaTime()*getSpeed());
	}
	
	public void moveDown() {
		setY(getY()-100*Gdx.graphics.getDeltaTime()*getSpeed());
	}
}
