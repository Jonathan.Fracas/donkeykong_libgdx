package modele;

public class Banana extends MovingObject{

	public Banana(float speed,float x,float y) {
		super(speed);
		rectangle.x = x;
		rectangle.y = y;
		rectangle.height = 30;
		rectangle.width = 30;
	}

	@Override
	public String getName() {
		return "banana.png";
	}

}
