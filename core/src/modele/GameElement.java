package modele;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.io.*;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Mario.class, name = "Mario"),
        @JsonSubTypes.Type(value = DonkeyKong.class, name = "DonkeyKong"),
        @JsonSubTypes.Type(value = Platform.class, name = "Platform"),
        @JsonSubTypes.Type(value = SingleLadder.class, name = "SingleLadder")})
public abstract class GameElement {
	protected Rectangle rectangle;
	
	GameElement(){
		rectangle = new Rectangle();
	}
	
	public Rectangle getRectangle() {
		return rectangle;
	}
	
	public void setRectangle(Rectangle rectangle) {
		this.rectangle = rectangle;
	}
	
	public void setRectangle(float x, float y, float height, float width) {
		setX(x);
		setY(y);
		rectangle.height = height;
		rectangle.width = width;
	}
	
	
	public float getHeight() {
		return rectangle.height;
	}
	public float getWidth() {
		return rectangle.width;
	}
	public float getX() {
		return rectangle.x;
	}
	public float getY() {
		return rectangle.y;
	}
	
	public void setX(float x) {
		rectangle.x = x;
	}
	
	public void setY(float y) {
		rectangle.y = y;
	}
	
	public abstract String getName();
	
}
