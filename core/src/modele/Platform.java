package modele;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import vue.TextureFactory;

public class Platform extends UnmovingObject {
	
	@JsonCreator
    public Platform(
      @JsonProperty("x") float x, 
      @JsonProperty("y") float y, 
      @JsonProperty("width") float width, 
      @JsonProperty("height") float height,
      @JsonProperty("number") int number) {
        super(number);
        rectangle.x=x;
		rectangle.y=y;
		rectangle.width = width;
		rectangle.height =height;
    }
	
	
	public String getName() {
		return "Platform"+number+".png";
	}
}
