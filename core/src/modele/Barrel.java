package modele;

import com.badlogic.gdx.Gdx;

public class Barrel extends MovingObject{

	private int number;
	private double angle;
	
	public Barrel(int number,float speed,float x,float y) {
		super(speed);
		this.number = number;
		rectangle.x = x;
		rectangle.y = y;
		rectangle.height = 75;
		rectangle.width = 75;
	}
	
	public Barrel(int number,float speed,float x,float y,double angle) {
		super(speed);
		this.number = number;
		this.angle = angle;
		rectangle.x = x;
		rectangle.y = y;
		rectangle.height = 75;
		rectangle.width = 75;
	}
	
	public void moveWithAngle() {
		setY(getY()-100*Gdx.graphics.getDeltaTime()*getSpeed()*(float)Math.sin(angle));
		setX(getX()+100*Gdx.graphics.getDeltaTime()*getSpeed()*(float)Math.cos(angle));
	}

	@Override
	public String getName() {
		if(number ==1 ) {
			return "BarrelTop.png";			
		}
		else {
			return "BarrelSide.png";
		}
	}

}
