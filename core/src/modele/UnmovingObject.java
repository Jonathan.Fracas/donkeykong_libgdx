package modele;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;

public abstract class UnmovingObject extends GameElement {
	int number;
	
	
	public UnmovingObject(int number) {
		this.number = number;
	}
	
	public int getNumber() {
		return number;
	}
	
	
}
