package vue;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.MyGdxGame;

import controleur.WorldRenderer;

public class GameScreen implements Screen {
	SpriteBatch batch;
	WorldRenderer WorldRenderer;
	MyGdxGame Game;
	
	public GameScreen(MyGdxGame game ) {
		batch =game.getSpriteBatch();
		Game = game;
		WorldRenderer = new WorldRenderer(game);
	}
	
	@Override
	public void show() {}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		
		batch.end();
		WorldRenderer.render(batch);
	}
	
	@Override
	public void resize(int width, int height) {
	
	}
	
	@Override
	public void pause() {

		
	}
	@Override
	public void resume() {

		
	}
	@Override
	public void hide() {

		
	}
	
	@Override
	public void dispose() {
		TextureFactory.getInstance().dispose();
	}

}
