package vue;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

public final  class TextureFactory {
	
	private static Texture mario2Texture = new Texture("MarioIdleLeft.png");
	private static Texture mario1Texture = new Texture("MarioIdleRight.png");
	private static Texture donkeyKongTexture = new Texture("DonkeyKong.png");
	private static Texture platform1Texture = new Texture("Platform1.png");
	private static Texture platform2Texture = new Texture("Platform2.png");
	private static Texture singleLadder1Texture = new Texture("SingelLadder1.png");
	private static Texture singleLadder2Texture = new Texture("SingelLadder2.png");
	private static Texture bananaTexture = new Texture("banana.png");
	private static Texture hammerTexture = new Texture("hammer.png");
	private static Texture barrelTexture = new Texture("BarrelTop.png");
	private static Texture barrelSideTexture = new Texture("BarrelSide.png");
	private static Texture donkeyKong1Texture = new Texture("dktitle01.png");
	private static Texture donkeyKong2Texture = new Texture("dktitle02.png");
	private static Texture donkeyKong3Texture = new Texture("dktitle03.png");
	private static Texture donkeyKong4Texture = new Texture("dktitle04.png");
	private static Texture donkeyKong5Texture = new Texture("dktitle05.png");
	private static Texture donkeyKong6Texture = new Texture("dktitle06.png");
	private static Texture marioUp4Texture = new Texture("mario_climbing4.png");
	private static Texture marioUp5Texture = new Texture("mario_climbing5.png");
	private static Texture chestTexture = new Texture("Chest000.png");
	
	

	private static Map<String, Texture> textures= new HashMap<String, Texture>();
	
	private static Texture heart4Texture = new Texture("4.png");
	private static Texture tinyMonkeyTexture = new Texture("TinyMonkey.png");
	
	private static Map<String, Texture> explosionTextures= new HashMap<String, Texture>();
	private static Texture explosion0Texture = new Texture("tile000.png");
	private static Texture explosion1Texture = new Texture("tile001.png");
	private static Texture explosion2Texture = new Texture("tile002.png");
	private static Texture explosion3Texture = new Texture("tile003.png");
	
	
	
    private static TextureFactory INSTANCE = null;
    
 


    public static TextureFactory getInstance() {
    	if(INSTANCE == null) {
    		INSTANCE = new TextureFactory();
    		textures.put("MarioIdleLeft.png",mario2Texture);
    		textures.put("MarioIdleRight.png",mario1Texture);
    		textures.put("Platform1.png", platform1Texture);
    		textures.put("Platform2.png", platform2Texture);
    		textures.put("SingelLadder1.png", singleLadder1Texture);
    		textures.put("SingelLadder2.png", singleLadder2Texture);
    		textures.put("DonkeyKong.png", donkeyKongTexture);
    		textures.put("banana.png", bananaTexture);
    		textures.put("BarrelTop.png", barrelTexture);
    		textures.put("hammer.png", hammerTexture);
    		textures.put("BarrelSide.png", barrelSideTexture);
    		textures.put("dktitle01.png", donkeyKong1Texture);
    		textures.put("dktitle02.png", donkeyKong2Texture);
    		textures.put("dktitle03.png", donkeyKong3Texture);
    		textures.put("dktitle04.png", donkeyKong4Texture);
    		textures.put("dktitle05.png", donkeyKong5Texture);
    		textures.put("dktitle06.png", donkeyKong6Texture);
    		textures.put("mario_climbing4.png", marioUp4Texture);
    		textures.put("mario_climbing5.png", marioUp5Texture);
    		textures.put("Chest000.png", chestTexture);
    		
    		explosionTextures.put("tile000.png", explosion0Texture);
    		explosionTextures.put("tile001.png", explosion1Texture);
    		explosionTextures.put("tile002.png", explosion2Texture);
    		explosionTextures.put("tile003.png", explosion3Texture);
    		
    	}
        return INSTANCE;
    }
    
	
	public Texture getTexture(String nom) {
		return textures.get(nom);
	}
	
	public Texture getTextureBackground() {
		return new Texture(Gdx.files.internal("background.png"));
	}
	
	public Texture getHeart() {
		return heart4Texture;
	}
	
	public Texture getTinyMonkey() {
		return tinyMonkeyTexture;
	}
	
	public Texture getTextureExplosion(String nom) {
		return explosionTextures.get(nom);
	}


	public void dispose() {
		mario1Texture.dispose();
		mario2Texture.dispose();
		singleLadder1Texture.dispose();
		singleLadder2Texture.dispose();
		platform1Texture.dispose();
		platform2Texture.dispose();
		bananaTexture.dispose();
		barrelTexture.dispose();
		barrelSideTexture.dispose();
		hammerTexture.dispose();
		donkeyKong1Texture.dispose();
		donkeyKong2Texture.dispose();
		donkeyKong3Texture.dispose();
		donkeyKong4Texture.dispose();
		donkeyKong5Texture.dispose();
		donkeyKong6Texture.dispose();
		marioUp4Texture.dispose();
		marioUp5Texture.dispose();
		tinyMonkeyTexture.dispose();

		heart4Texture.dispose();
		explosion1Texture.dispose();
		explosion2Texture.dispose();
		explosion3Texture.dispose();
	}
	
	

}
