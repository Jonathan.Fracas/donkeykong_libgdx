package vue;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.MyGdxGame;

public class SuccessScreen implements Screen {

	SpriteBatch batch;
	MyGdxGame Game;
	private BitmapFont font = new BitmapFont();
	
	public SuccessScreen(MyGdxGame game ) {
		batch =game.getSpriteBatch();
		Game = game;
	}
	
	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		font.draw (batch, "Gagne win winner ", Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2);
		 if(Gdx.input.isKeyJustPressed(Keys.ENTER)) {
			 Game.setScreen(new GameScreen(Game));
		   }
		batch.end();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
